FROM python:3.9
RUN mkdir /app 
COPY / /app
COPY pyproject.toml /app 
WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD} 
EXPOSE 8887
RUN pip3 install poetry
RUN poetry install --no-dev
ENTRYPOINT [ "poetry", "run", "all" ]