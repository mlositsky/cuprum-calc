# #!/usr/bin/env python

import os

from flask import Flask
from flask.helpers import send_from_directory


def create_app():
    app = Flask(
        __name__,
        root_path=os.path.join(os.getcwd()),
        static_folder="/",
        static_url_path="/",
    )

    @app.route("/cuprum/<path:path>")
    @app.route("/cuprum/")
    @app.route("/<path:path>")
    @app.route("/")
    def root(path="index.html"):
        return send_from_directory(".", path)

    return app


def start():
    os.chdir("front")  # TODO abs path
    print(f"Changed dir: {os.getcwd()}")
    app = create_app()
    app.run(
        debug=False,
        host="127.0.0.1",
        port=8887,
    )


if __name__ == "__main__":
    start()
