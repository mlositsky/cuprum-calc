import os

import pandas as pd
from pretty_html_table import build_table

PATH_TO_ORIGINAL_DATA = "data/"
PATH_TO_FRONT_DATA = "front/data/"


def main(
    original=PATH_TO_ORIGINAL_DATA,
    front=PATH_TO_FRONT_DATA,
):
    li = []
    for filename in os.listdir('data'):
        csv_data = pd.read_csv(original + filename, encoding="utf-8", comment="#")
        li.append(csv_data)
    all_csv_data = pd.concat(li, axis=0, ignore_index=True)
    all_csv_data.to_json(
        front + "copper_nutrition.json", orient="records", indent=2, force_ascii=False
    )
    html_table_blue_light = build_table(csv_data, "blue_light")
    with open(front + "pandas_table.html", "w") as f:
        f.write(html_table_blue_light)


def start():
    os.makedirs(PATH_TO_FRONT_DATA, exist_ok=True)
    main(PATH_TO_ORIGINAL_DATA, PATH_TO_FRONT_DATA)


if __name__ == "__main__":
    start()
