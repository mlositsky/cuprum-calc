from collections import namedtuple

Row = namedtuple("Row", "product copper_content data_source")
