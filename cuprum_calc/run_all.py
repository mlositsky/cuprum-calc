#!/usr/bin/env python

from cuprum_calc import prepare_data, run_server


def start():
    prepare_data.start()
    run_server.start()


if __name__ == "__main__":
    start()
