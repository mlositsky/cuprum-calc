#!/usr/bin/env python

import pandas as pd
from cuprum_calc.utils import Row
from requests_html import HTMLSession

session = HTMLSession()


def crawl():
    food_page = session.get("https://fitaudit.ru/food/abc")
    food_urls = [
        x for x in food_page.html.links if x.startswith("https://fitaudit.ru/food/")
    ]
    food_urls.sort()
    food_urls_qty = len(food_urls)
    print(f"{food_page=} {food_urls_qty=}")
    for i, url in enumerate(food_urls, start=1):
        product_page = session.get(url)
        print(f"{url=}")
        product = product_page.html.xpath(
            "//*[contains(@class, 'flhead_item__content')]"
        )[0].text.replace("\n", " ")
        print(f"{product=}")
        object_w_copper = product_page.html.xpath(
            "//*[contains(@class, 'tbl-chart pr__copper tip')]"
        )
        print(f"{object_w_copper=}")
        copper_percent_raw = object_w_copper[0].attrs["tip"]
        print(f"{copper_percent_raw=}")
        copper_content = float(copper_percent_raw.strip("%")) * 10
        print(f"{copper_content=}")
        food_urls_left = food_urls_qty - i
        print(f">>>{food_urls_left=}")
        yield Row(product, copper_content, "fitaudit.ru")


if __name__ == "__main__":
    result = []
    for row in crawl():
        result.append(row)
    pd.DataFrame(result).to_csv("data/fitaudit.csv", index=False, header=False)
